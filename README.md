# 
Candy, candy -- he makes so much! Uncle Sweetshare has a magic touch! So it's back to the workshop in the snow! With lovely lanterns all aglow! He he! Ha ho! He he he ha ha ho!

- Candy, candy -- he makes so much! Uncle Sweetshare has a magic touch!

- Uncle Sweetshare is coming near, to spread his candy and his cheer!

- So it's back to the workshop in the snow! With lovely lanterns all aglow! He he! Ha ho! He he he ha ha ho!

- It's better than trinkets, games or toys! So say all the little girls and boys!

- When the sugar is warmed by the pale hearth light, the happiness spreads throughout the night!

- My Uncle's candy is so sweet! It's such a yummy winter's treat!

- He he! Ha ho! To the worskhop he will go!