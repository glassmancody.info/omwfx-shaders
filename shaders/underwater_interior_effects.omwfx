sampler_3d water_nrm {
    source = "shaders/textures/water_nrm.dds";
    mag_filter = linear;
    min_filter = linear;
    wrap_s = repeat;
    wrap_t = repeat;
    wrap_r = repeat;
}

shared {
    float lf = (0.2 + 0.8*omw.sunVis) * (0.32*omw.sunColor.r + 0.47*omw.sunColor.g + 0.21*omw.sunColor.b);
    const vec4 lightcolour = vec4(0.6, 0.91, 1.0, 0.0);
}

fragment wobble {
    omw_In vec2 omw_TexCoord;

    vec3 toWorld(vec2 tex)
    {
        vec3 v = vec3(omw.viewMatrix[0][2], omw.viewMatrix[1][2], omw.viewMatrix[2][2]);
        v += vec3(1/omw.projectionMatrix[0][0] * (2*(1 - tex.x)-1)) * vec3(omw.viewMatrix[0][0], omw.viewMatrix[1][0], omw.viewMatrix[2][0]);
        v += vec3(-1/omw.projectionMatrix[1][1] * (2*tex.y-1)) * vec3(omw.viewMatrix[0][1], omw.viewMatrix[1][1], omw.viewMatrix[2][1]);
        return v;
    }

    float getLinearDepth(in vec2 tex)
    {
        float d = omw_GetDepth(tex);
        float ndc = d * 2.0 - 1.0;

        return omw.near * omw.far / (omw.far + ndc * (omw.near - omw.far));
    }

    void main()
    {
        //FIXME: wobble clamping
        vec2 wobble = 0.01 * (2 * texture(water_nrm, vec3(omw_TexCoord, 0.2*omw.simulationTime)).rg - 1);
        // wobble *= 1 - pow(2*omw_TexCoord - 1, vec2(32));
        wobble += omw_TexCoord;

        vec4 c = omw_GetLastShader(wobble);

        float d = getLinearDepth(wobble);
        vec3 v = -d * toWorld(wobble);
        v += omw.eyePos.xyz;

        float k = 1 - texture(water_nrm, vec3(v.xy / 1783, 0.4 * omw.simulationTime)).b;

        c *= 1 - 0.2 * step(v.z, omw.waterHeight - 1) * clamp(exp((v.z - omw.waterHeight)/100), 0.0, 1.0) * k;

        omw_FragColor = c;
    }
}

technique {
    passes = wobble;
    description = "Underwater interior effects";
    author = "Hrnchamd";
    version = "1.0";
    flags = Disable_AboveWater, Disable_Exteriors;
}
